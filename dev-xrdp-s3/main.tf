locals {
  folder_id      = var.YC_FOLDER_ID
  cloud_id       = var.YC_CLOUD_ID
  token_id       = var.YC_TOKEN
  zone_id        = "ru-central1-a"
  sa_name        = "sa-dev-prof"
  sa_desc        = "Service account for devprof S3 bucket"
  bucket_name    = "dev-prof"
  bucket_desc    = "Bucket for prod service account"
  statickey_desc = "static access key for object storage"
}


provider "yandex" {
  token     = local.token_id
  cloud_id  = local.cloud_id
  folder_id = local.folder_id
  zone      = local.zone_id
}

terraform {
  required_providers {
    yandex = {
      source = "terraform-registry.storage.yandexcloud.net/yandex-cloud/yandex"
    }
  }
}

resource "yandex_iam_service_account" "sa" {
  name = local.sa_name
  description = local.sa_desc
}

// Назначение роли сервисному аккаунту
resource "yandex_resourcemanager_folder_iam_member" "sa-editor" {
  folder_id = local.folder_id
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.sa.id}"
}

// Создание статического ключа доступа
resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = yandex_iam_service_account.sa.id
  description        = local.statickey_desc
}

// Создание бакета с использованием ключа
resource "yandex_storage_bucket" "dev-prof" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket     = local.bucket_name
}
