# Provider

provider "yandex" {
  token     = local.token_id
  cloud_id  = local.cloud_id
  folder_id = local.folder_id
  zone      = local.zone_id
}

