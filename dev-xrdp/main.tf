locals {
  folder_id          = var.YC_FOLDER_ID
  cloud_id           = var.YC_CLOUD_ID
  token_id           = var.YC_TOKEN
  zone_id            = "ru-central1-a"
  sa_name            = "sa-xrdp"
  sa_desc            = "Service account for devprof S3 bucket"
  vm_name            = "dev-xrdp-vm"
  vm_fedora_image_id = "fd8enmf694d91navv4vk"
  vm_ubuntu_image_id = "fd8ingbofbh3j5h7i8ll"
  vm_toolbox_image_id= "fd88piqslhj0th16uage"
  cpu_cores          = "4"
  cpu_memory         = "4"
  cpu_fraction       = "100"
  bucket_name        = "dev-prof"
  bucket_desc        = "Bucket for dev-prof service account"
  statickey_desc     = "static access key for object storage"
}

# Service Account


resource "yandex_iam_service_account" "sa-xrdp" {
  name        = local.sa_name
  description = "K8S zonal service account"
}
resource "yandex_resourcemanager_folder_iam_member" "sa-xrdp" {
  # Сервисному аккаунту назначается роль "k8s.clusters.admin".
  folder_id = local.folder_id
  role      = "admin"
  member    = "serviceAccount:${yandex_iam_service_account.sa-xrdp.id}"
}

data "yandex_compute_image" "ubuntu-22-04" {
  family = "ubuntu-2204-lts"
}

data "template_file" "cloud_init" {
  template = file("meta.txt")
  vars = {
    user    = var.USER
    ssh_key = file(var.PUBLIC_KEY_PATH)
  }
}
# instances

resource "yandex_compute_instance" "dev-xrdp-vm" {
  name = local.vm_name
  zone = local.zone_id

  resources {
    cores         = local.cpu_cores
    memory        = local.cpu_memory
    core_fraction = local.cpu_fraction
  }

  boot_disk {
    initialize_params {
      image_id = local.vm_toolbox_image_id
      size     = 64
      type     = "network-ssd"

    }
  }

  scheduling_policy {
    preemptible = true
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.xrdp-subnet-a.id
    nat       = true
  }



  metadata = {
    user-data          = data.template_file.cloud_init.rendered
    serial-port-enable = 1
  }
  provisioner "remote-exec" {
    inline = [
      "echo '${var.USER}:${var.XRDP_PASSWORD}' | sudo chpasswd",
      "sudo apt-get update -y",
      "sudo DEBIAN_FRONTEND=noninteractive apt-get install xrdp tightvncserver ubuntu-desktop -y",
      "sudo systemctl enable xrdp",
      "sudo ufw allow 3389/tcp",
      "sudo /etc/xrdp/startwm.sh",
      "sudo /etc/init.d/xrdp restart"
      "sudo DEBIAN_FRONTEND=noninteractive apt-get install docker-compose-plugin mc git fish -y",
      "curl -fOL https://github.com/coder/code-server/releases/download/v4.6.0/code-server_4.6.0_amd64.deb",
      "sudo dpkg -i code-server_4.6.0_amd64.deb",
      "sudo systemctl enable --now code-server@${var.USER}"
    ]
    connection {
      type        = "ssh"
      user        = var.USER
      private_key = file(var.PRIVATE_KEY_PATH)
      host        = self.network_interface[0].nat_ip_address

    }

  }
}