terraform {
  backend "s3" {
    endpoint = "storage.yandexcloud.net"
    bucket   = "dev-xrdp"
    region   = "ru-central1-a"
    key      = "terraform.tfstate.d/stage/terraform.tfstate"
    # access_key = var.AWS_ACCESS_KEY_ID_XRDP
    # secret_key = var.AWS_SECRET_ACCESS_KEY_ID_XRDP
    skip_region_validation      = true
    skip_credentials_validation = true
  }
  #    backend "local" {}
}
