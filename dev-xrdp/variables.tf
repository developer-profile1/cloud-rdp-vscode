#--------------------------------------------------------------------------------
#Variable section
#--------------------------------------------------------------------------------
variable "YC_CLOUD_ID" {
  type = string
}
variable "YC_FOLDER_ID" {
  type = string
}
variable "YC_TOKEN" {
  type = string
}

variable "PUBLIC_KEY_PATH" {
  description = "Path to ssh public key, which would be used to access workers"
  default     = "~/.ssh/id_ed25519.pub"
}

variable "PRIVATE_KEY_PATH" {
  description = "Path to ssh private key, which would be used to access workers"
  default     = "~/.ssh/id_ed25519"
}

variable "XRDP_PASSWORD" {
  type = string
}

variable "USER" {
  type    = string
  default = "rdpusr"
}

variable "AWS_ACCESS_KEY_ID_XRDP" {
  type = string
}

variable "AWS_SECRET_ACCESS_KEY_XRDP" {
  type = string
}