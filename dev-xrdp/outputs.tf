# outputs

output "cp-internal-ip" {
  value = yandex_compute_instance.dev-xrdp-vm.network_interface.0.ip_address
}

output "cp-external-ip" {
  value = yandex_compute_instance.dev-xrdp-vm.network_interface.0.nat_ip_address
}

# --- network/outputs.tf --- #

# output "yandex_vpc_network" {
#   description = "Yandex.Cloud VPC Network"
#   value       = data.yandex_vpc_network.devnet.id
# }

# output "yandex_vpc_subnets" {
#   description = "Yandex.Cloud Subnets map"
#   value       = data.yandex_vpc_subnet.devsubnet.id
# }